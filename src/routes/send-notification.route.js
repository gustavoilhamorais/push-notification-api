const express          = require("express");
const router           = express.Router();
const useWebPush       = require("../web-push.conf");
const createConnection = require("../database/connection");
const subscriptions    = require("../database/subscriptions");

router.get("/", async (request, response) => {
  try {
    const dbConnection = createConnection();
    const data         = await subscriptions.getAll(dbConnection);
    if (data === null) {
      await dbConnection.close();
      return response.status(404)
        .json({
          status: 404,
          statusText: "error",
          message: "No subscriptions were found.",
          data: null
        });
    }
    const webpush = useWebPush();
    await data.forEach(subscription => {
      webpush.sendNotification(subscription, "Hello!");
    });
    await dbConnection.close();
    return response.status(200)
      .json({ 
        status: 200,
        statusText: "success",
        message: "Notifications were successfully sent.",
        data: null
      });
  } catch (error) {
    console.log(error);
  }
});

module.exports = router;
