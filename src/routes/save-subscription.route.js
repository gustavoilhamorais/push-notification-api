const express          = require("express");
const router           = express.Router();
const subscriptions    = require("../database/subscriptions");
const createConnection = require("../database/connection");

router.post("/", async (request, response) => {
  const subscription = request.body;
  const connection   = createConnection();
  const dbResponse   = await subscriptions.create(connection, subscription);
  if (dbResponse === true) {
    return response.status(200)
      .json({
        status: 200,
        statusText: "success",
        message: "success",
        data: null
      });
  }
  return response.status(500)
    .json({
      status: 500,
      statusText: "error",
      message: "Internal Error",
      data: null
    });
});

module.exports = router;
