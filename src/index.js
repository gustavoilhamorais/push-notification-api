const express = require("express");
const cors    = require("cors");
const app     = express();
const https   = require("https");
const fs      = require("fs");

const port    = process.env.PORT || 8080;
const ssl     = process.env.SSL_PATH;
const options = {
  key:  fs.readFileSync(`${ssl}/privkey.pem`, "utf-8"),
  cert: fs.readFileSync(`${ssl}/cert.pem`,    "utf-8"),
  ca:   fs.readFileSync(`${ssl}/chain.pem`,   "utf-8"),
};

app.use(express.urlencoded({ extended: true }));
app.use(express.json());
app.use((request, response, next) => {
  response.setHeader("Content-Type", "application/json");
  response.header("Access-Control-Allow-Origin", process.env.CORS_ALLOW_ORIGIN);
  response.header("Access-Control-Allow-Methods", "GET,PUT,POST,DELETE");
  response.header(
    "Access-Control-Allow-Headers",
    "Content-Type, Access-Control-Allow-Headers, Authorization, X-Requested-With"
  );
  app.use(cors({ origin: process.env.CORS_ALLOW_ORIGIN }));
  next();
});

app.get("/", (req, res) => res.status(401).send("401 Unauthorized"));

app.use("/save-subscription", require("./routes/save-subscription.route"));
app.use("/send-notification", require("./routes/send-notification.route"));

https.createServer(options, app)
  .listen(port, () => console.log(`Push Notification API listening on port ${port}!`));
