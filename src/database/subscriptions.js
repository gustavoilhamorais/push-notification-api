async function create(connection, subscription) {
  const database   = connection.db(process.env.APP_NAME);
  const collection = database.collection("subscriptions");
  const result     = await collection.insertOne(subscription);
  console.log(`New subscription was created with the _id: ${result.insertedId}`);
  await connection.close();
  return true;
}

async function getAll(connection) {
  const database   = connection.db(process.env.APP_NAME);
  const collection = database.collection("subscriptions");
  const cursor     = collection.find();
  if ((await cursor.count()) === 0) {
    console.log("No documents found!");
    return null;
  }
  return await cursor;
}

module.exports = { create, getAll };
