const webpush = require("web-push");

const vapidKeys = {
  publicKey:  process.env.NOTIFICATION_SERVER_PUBLIC_KEY,
  privateKey: process.env.NOTIFICATION_SERVER_PRIVATE_KEY,
}

const useWebPush = () => {
  webpush.setVapidDetails(
    "mailto:myuserid@email.com", 
    vapidKeys.publicKey, 
    vapidKeys.privateKey
  );
  return webpush;
}

module.exports = useWebPush;