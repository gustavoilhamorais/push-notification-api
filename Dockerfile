FROM node:lts-alpine

WORKDIR /usr/share/api

COPY ./package.json /usr/share/api/

RUN npm install

EXPOSE 8080
CMD ["npm", "start"]
