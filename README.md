# Push Notification API

## Author

[Gustavo Ilha Morais](https://gustavoilhamorais.com.br)

## Technologies

- [NodeJS](https://nodejs.org )
- [ExpressJS](https://expressjs.com)
- [MongoDB](https://mongodb.com)
- [Web-Push](https://github.com/web-push-libs/web-push#readme)

## Description

Simple web server created using [ExpressJS](https://expressjs.com), that integrates a NoSQL database ([MongoDB](https://mongodb.com)) and [Web-Push](https://github.com/web-push-libs/web-push#readme) services.

Currently it has:
  * a route that saves a ServiceWorker subscription
  * a route that when triggered, sends a default notification to all subscriptions in the DB collection.


## LICENSE
MPL-2.0